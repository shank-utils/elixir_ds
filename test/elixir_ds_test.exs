defmodule ElixirDSTest do
  use ExUnit.Case

  alias ElixirDS.Deque

  doctest Deque

  test "create an empty deque" do
    dq = Deque.new()
    assert Deque.size(dq) == 0
  end

  test "create a deque from a list" do
    dq = Deque.from_list([1, 2, 3])
    assert Deque.size(dq) == 3
    assert Deque.peek_front(dq) == 1
    assert Deque.peek_back(dq) == 3
  end

  test "peek from an empty deque returns :error" do
    assert :error == Deque.new() |> Deque.peek_front()
    assert :error == Deque.new() |> Deque.peek_back()
  end

  test "peek_front returns the value for a non empty deque" do
    assert 1 == Deque.from_list([1, 2]) |> Deque.peek_front()
  end

  test "peek_back returns the value for a non empty deque" do
    assert 2 == Deque.from_list([1, 2]) |> Deque.peek_back()
  end

  test "push_front adds to the front of the deque" do
    dq = Deque.from_list([3, 4]) |> Deque.push_front(2)
    assert 2 == Deque.peek_front(dq)
    assert Deque.size(dq) == 3
  end

  test "push_back adds to the back of the deque" do
    dq = Deque.from_list([3, 4]) |> Deque.push_back(2)
    assert 2 == Deque.peek_back(dq)
    assert Deque.size(dq) == 3
  end

  test "pop_front removes an item from the front of the deque" do
    dq = Deque.from_list([1, 2, 3])
    {f, dq} = Deque.pop_front(dq)
    assert f == 1
    assert Deque.size(dq) == 2
    assert Deque.peek_front(dq) == 2
    assert Deque.peek_back(dq) == 3
  end

  test "pop_front retuns the front element even if the front list in the deque is empty" do
    dq = %Deque{front: [], back: [3, 2, 1]}
    {val, dq} = Deque.pop_front(dq)
    assert val == 1
    assert Deque.size(dq) == 2
    assert Deque.peek_front(dq) == 2
    assert Deque.peek_back(dq) == 3
  end

  test "pop_front returns :error in case the deque is empty" do
    assert :error == Deque.new() |> Deque.pop_front()
  end

  test "pop_back removes an item from the back of the deque" do
    dq =
      Deque.new()
      |> Deque.push_back(1)
      |> Deque.push_back(2)
      |> Deque.push_back(3)

    {b, dq} = Deque.pop_back(dq)
    assert b == 3
    assert Deque.size(dq) == 2
    assert Deque.peek_front(dq) == 1
    assert Deque.peek_back(dq) == 2
  end

  test "pop_back retuns the back element even if the back list in the deque is empty" do
    dq = %Deque{back: [], front: [1, 2, 3]}
    {val, dq} = Deque.pop_back(dq)
    assert val == 3
    assert Deque.size(dq) == 2
    assert Deque.peek_back(dq) == 2
    assert Deque.peek_front(dq) == 1
  end

  test "pop_back returns :error in case the deque is empty" do
    assert :error == Deque.new() |> Deque.pop_back()
  end

  test "rotating by one from front to back" do
    dq = Deque.from_list([1, 2, 3])
    rotated = Deque.rotate(dq, 1)
    assert Deque.size(rotated) == 3
    assert Deque.peek_front(rotated) == 3
    assert Deque.peek_back(rotated) == 2
  end

  test "rotating multiple positions from front to back" do
    dq = Deque.from_list([1, 2, 3])
    rotated = Deque.rotate(dq, 2)
    assert Deque.size(rotated) == 3
    assert Deque.peek_front(rotated) == 2
    assert Deque.peek_back(rotated) == 1
  end

  test "rotating by one from back to front" do
    dq = Deque.from_list([1, 2, 3])
    rotated = Deque.rotate(dq, -1)
    assert Deque.size(rotated) == 3
    assert Deque.peek_front(rotated) == 2
    assert Deque.peek_back(rotated) == 1
  end

  test "rotating multiple positions from back to front" do
    dq = Deque.from_list([1, 2, 3])
    rotated = Deque.rotate(dq, -2)
    assert Deque.size(rotated) == 3
    assert Deque.peek_front(rotated) == 3
    assert Deque.peek_back(rotated) == 2
  end
end
