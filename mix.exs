defmodule ElixirDS.MixProject do
  use Mix.Project

  @gitlab_url "https://gitlab.com/shank-utils/elixir_ds"

  def project do
    [
      app: :elixir_ds,
      version: "0.1.1",
      elixir: ">= 1.4.0",
      start_permanent: Mix.env() == :prod,
      description: "Datastructures for Elixir",
      package: package(),
      deps: deps(),
      aliases: aliases(),
      source_url: @gitlab_url,
      homepage_url: @gitlab_url
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.19.2", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      licenses: ["MIT"],
      links: %{"Gitlab" => @gitlab_url}
    ]
  end

  defp aliases do
    [test: "test --color --trace"]
  end
end
