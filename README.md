# Elixir Datastructures

[![pipeline status](https://gitlab.com/shank-utils/elixir_ds/badges/master/pipeline.svg)](https://gitlab.com/shank-utils/elixir_ds/commits/master)

Datastructures for Elixir
- [x] Deque
- [ ] Zipper List

## Installation

Package is [available in Hex](https://hex.pm/packages/elixir_ds) and can be installed by adding `elixir_ds` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:elixir_ds, "~> 0.1.0"}
  ]
end
```

Documentation can be found at [https://hexdocs.pm/elixir_ds](https://hexdocs.pm/elixir_ds).

